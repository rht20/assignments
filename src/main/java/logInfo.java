package main.java;

/**
 * @author rakibul.hasan
 * @since 2/4/20
 */
public class logInfo {
    public int time,gCnt,pCnt,uriCnt,responseTime;
    public String uri;

    public void setTime(int time) {
        this.time = time;
    }

    public void setgCnt(int gCnt) {
        this.gCnt = gCnt;
    }

    public void setpCnt(int pCnt) {
        this.pCnt = pCnt;
    }

    public void setUriCnt(int uriCnt) {
        this.uriCnt = uriCnt;
    }

    public void setResponseTime(int responseTime) {
        this.responseTime = responseTime;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getTime() {
        return time;
    }

    public int getgCnt() {
        return gCnt;
    }

    public int getpCnt() {
        return pCnt;
    }

    public int getUriCnt() {
        return uriCnt;
    }

    public int getResponseTime() {
        return responseTime;
    }

    public String getUri() {
        return uri;
    }

    public void logInfoView() {
        int time = getTime();
        if(time<12) System.out.print(time+".00 am - "+(time+1)+".00 am");
        else if(time==24)   System.out.print("12.00 am - 1.00 am");
        else
        {
            time %= 12;
            if(time==0) time = 12;
            System.out.print(time+".00 pm - "+(time+1)%12+".00 pm");
        }

        System.out.println(" | "+getgCnt()+"/"+getpCnt()+" | "+getUriCnt()+" | "+getResponseTime());
    }
}
