package main.java;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 * @author rakibul.hasan
 * @since 2/4/20
 */

public class LogParser {
    public static void main(String[] args) throws IOException {
        processLogData("log-parser-sample-file.log");
    }

    public static void processLogData(String file) throws IOException {
        FileReader FR = new FileReader(file);
        Scanner scan = new Scanner(FR);

        logInfo[] logInfoList = new logInfo[30];
        Set<String>[] stUri = new HashSet[30];
        logInfo info;

        for(int i=1; i<25; i++) {
            logInfoList[i] = new logInfo();
            logInfoList[i].setTime(i);
            stUri[i] = new HashSet<String>();
            stUri[i].clear();
        }

        int time, c;
        while(scan.hasNext()) {
            String log = scan.nextLine();
            info = processLog(log);

            time = info.getTime();
            if(time==-1) {
                continue;
            }

            c = logInfoList[time].getgCnt();
            logInfoList[time].setgCnt(c + info.getgCnt());
            c = logInfoList[time].getpCnt();
            logInfoList[time].setpCnt(c + info.getpCnt());
            c = logInfoList[time].getResponseTime();
            logInfoList[time].setResponseTime(c + info.getResponseTime());
            stUri[time].add(info.getUri());
        }

        for(int i=1; i<25; i++) logInfoList[i].setUriCnt(stUri[i].size());

        System.out.println("Time | " + "GET/POST Count | " + "Unique URI Count | " + "Total Response Time");
        for(int i=1; i<25; i++) logInfoList[i].logInfoView();
    }

    public static logInfo processLog(String log) {
        logInfo info = new logInfo();

        String[] logEntry = log.split(",");

        if(logEntry.length!=4) {
            info.setTime(-1);
            return info;
        }

        String temp = logEntry[2].split(" ")[1];

        if(isGP(temp)==false) {
            info.setTime(-1);
            return info;
        }
        if(temp.equals("G"))    info.setgCnt(info.getgCnt()+1);
        else info.setpCnt(info.getpCnt()+1);

        temp = logEntry[3].split("=")[1];

        for(int i=temp.length()-1; i>=0; i--) {
            if(temp.charAt(i)>='a' && temp.charAt(i)<='z')  continue;
            temp = temp.substring(0, i+1);
            info.setResponseTime(Integer.parseInt(temp));
            break;
        }

        temp = logEntry[0].split(" ")[1].split(":")[0];
        info.setTime(Integer.parseInt(temp));

        logEntry = logEntry[1].split(" ");
        temp = logEntry[logEntry.length-1].split("=")[1];
        info.setUri(temp);

        return info;
    }

    public static boolean isGP(String str) {
        if(str.length()==1 && (str.charAt(0)=='G' || str.charAt(0)=='P'))   return true;
        return false;
    }
}
